#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include "TTimer.h"
#include "TSystem.h"
#include <sstream>
#include <bits/stdc++.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include "TF1.h"
#include "TH1F.h"
#include <mpi.h>
#include "Rtypes.h"
#include "TGraph.h"
#include <sys/stat.h>
#include "TCanvas.h"
#include "TRandom3.h"
#include "TRandom.h"
using namespace ROOT;
using namespace std;

//--------------------------------------------------------------------------------------------------
//            Static Kubo-Toyabe functions in TF and ZF
//--------------------------------------------------------------------------------------------------
Double_t MuSRSignalTF(Double_t* x, Double_t* par) //External: TF; Internal: Gaussian field 
{
	Double_t t = x[0];
	Double_t tau = par[0];
	Double_t a0 = par[1];
	Double_t omega = par[2];
	Double_t sigma = par[3];
	
	return 1.+a0*cos(omega*t)*exp(-t*t*sigma*sigma/2.); 
}

Double_t MuSRSignalZF(Double_t* x, Double_t* par) //External: ZF; Internal: Gaussian field 
{
	Double_t t = x[0];
	Double_t tau = par[0];
	Double_t a0 = par[1];
	Double_t sigma = par[2];
	
	return 1.+a0*(1./3.+2./3.*(1.-sigma*sigma*t*t)*exp(-sigma*sigma*t*t/2)); 
}

 
int main(int argc, char** argv)
{
	// Creating a directory 
	if (mkdir("../data", 0777) == -1) 
		cerr << "Error :  " << strerror(errno) << endl; 
	else
		cout << "Directory created"<<endl;;
	
	//--------------------------------------------------------------------------------------------------
	//            Initilization of variables
	//--------------------------------------------------------------------------------------------------
	Double_t omega = 16.51616*7.; // angular frequency of an external TF: 100 Gauss = 16.51616
	Double_t tau = 2.1969811; // muon lifetime, unit: us
	Double_t A0 = 0.28; // asymmetry of a muSR spectrometer
	Double_t pulseWidth = 0.01; // pulse width in FWHM, unit = us
	Double_t bunchInterval = 0.41;// time interval between two bunches, unit = us
	Int_t muSRspectrum[120000] = {0}; // Forward detector
	Int_t muSRspectrum1[120000] = {0}; // Backward detector
	
	Double_t sigma = 1.; // depolarization rate of muons in an interal field with a Gaussian distribution, unit: us-1

	Double_t N_FB = 0.;
	Double_t t_Det = 0.;
	Int_t n_max=0;		
	
	pulseWidth  = 10.; // pulse width in FWHM, unit: ns
	
	
	
	//--------------------------------------------------------------------------------------------------
	//            setup of txt file output
	//--------------------------------------------------------------------------------------------------
	fstream file("../data/output.txt", ios::app|ios::out);


	//--------------------------------------------------------------------------------------------------
	//           Sampling function setup
	//--------------------------------------------------------------------------------------------------			
	TF1* f1 = new TF1("MuonDecayTimeF", MuSRSignalZF, 0, 24., 3); //sampling function in a time window of (0, 24 us)
	f1->SetParameters(tau, A0, sigma);
	f1->SetNpx(120000);
						
	for(Int_t i=0; i<120000; i++)
	{
		muSRspectrum[i] = 0;
		muSRspectrum1[i] = 0;
	}
			
	//--------------------------------------------------------------------------------------------------
	//           Sampling 
	//--------------------------------------------------------------------------------------------------
	const Int_t CountsPerCore = Int_t(100.E6); // total number of muons in a run
	for(Int_t i=0; i<CountsPerCore; i++) 
	{
		Int_t j = 0;
				
		N_FB = 0.;
		t_Det = 0.; //detected time of positrons
		Double_t t0 = 0.;
		Double_t t1 = 0.;
		

		//---------------------------------------------------------
		//            First pulse
		//---------------------------------------------------------		
		t0 = gRandom->Gaus(0., pulseWidth/2.355); // start time 
		t1 = gRandom->Exp(tau); // Decay time of muons
		t_Det = t0+t1; // total time of a positron from generation to detection by a counter
		j = Int_t(t_Det*1000*5);
		N_FB = f1->Eval(t1); 
		
		//determine which detector records the positron count
		Double_t randRatio = gRandom->Uniform()*2.;
		if(randRatio < N_FB)
		{
			if(j>=0 && j<120000)
			{
				muSRspectrum[j]++; // foreward detector
			}
		}
		else
		{
			if(j>=0 && j<120000)
			{
				muSRspectrum1[j]++; // backward detector
			}
		}
		
		
		//---------------------------------------------------------
		//            Second pulse
		//---------------------------------------------------------
		t0 = gRandom->Gaus(bunchInterval, pulseWidth/2.355); // start time in second pulse
		t1 = gRandom->Exp(tau); // Decay time of muons in second pulse
		t_Det = t0+t1; // total time of a positron from generation to detection by a counter
		j = Int_t(t_Det*1000*5);
		N_FB = f1->Eval(t1); 
		
		//determine which detector records the positron count
		randRatio = gRandom->Uniform()*2.;
		if(randRatio < N_FB)
		{
			if(j>=0 && j<120000)
			{
				muSRspectrum[j]++; // foreward detector
			}
		}
		else
		{
			if(j>=0 && j<120000)
			{
				muSRspectrum1[j]++; // backward detector
			}
		}
		
		
	}
			
			 
	delete f1;

		 
	//output data to txt files
	//bin width: 0.2 ns, 12000*0.2 ns = 24 us in total
	for(Int_t i=0; i<120000; i++)
	{
		if(muSRspectrum[i]!=0 || muSRspectrum1[i]!=0)
			file<<i*0.2/1000<<" "<<muSRspectrum[i]<<" "<<muSRspectrum1[i]<<endl; // //time unit: us
	}
			 
		
	return 0;
	 
}
	