This directory contains two sub-directories which can do single- and double- pulse simulations. These codes can be running on a Linux platform like CentOS, Ubuntu, etc.
Below are several steps for a successful run:

#### step 1: software pre-requisition 
Details can be found on the official website of ROOT
1) Intall ROOT-5.18 or higher
2) Setup the environment variable
   export ROOTSYS= /xxx (installation directory)
   export PATH= $ROOTSYS/bin:PATH
   export LD_LIBRARY_PATH=$ROOTSYS/lib:LD_LIBRARY_PATH

#### step 2: running
1) build a compiling directory
  $ mkdir build
  $ cd build
2) cmake and make
  $ cmake ..
  $ make
3) runing
After sub-setp 2), you will get exectuable files such as SinglePulseMuSR.exe, DoublePulseMuSR.exe
 ./SinglePulseMuSR 
 or ./DoublePulseMuSR

 If you need more details, please contact Dr. Ziwen Pan (panzw14@mail.ustc.edu.cn).
